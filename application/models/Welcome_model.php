<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function saveDirectory($data) {
        $res = 0;
		$query = $this->db->insert('file_records', $data);
		if($query){
			$res = 1;	
		}
		return $res;
    }

    public function getDirectory() {
        $this->db->select('*');
        $this->db->where('status', 1);
		$query	=	$this->db->get('file_records');
		$resultArr	=	array();
		if($query->num_rows() > 0){
			$resultArr	=	$query->result_array();		
		}
		return $resultArr;
    }

    public function deleteData($id) { 
        $res = 0;
		$this->db->where('id', $id);
		$query=$this->db->update('file_records',array("status"=> 0));
		if($query){
			$res = 1;	
		}
		return $res;
    }

    public function getdeletedDirectory() {
        $this->db->select('*');
        $this->db->where('status', 0);
		$query	=	$this->db->get('file_records');
		$resultArr	=	array();
		if($query->num_rows() > 0){
			$resultArr	=	$query->result_array();		
		}
		return $resultArr;
    }
}
