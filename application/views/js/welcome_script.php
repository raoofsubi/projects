<script>
		$(document).ready( function () {
    		$('#myTable').DataTable();
            var add_validation = 0;
			$("form#add_dirctory_form").validate({
    		    rules: {
    		        name: { required: true,
							remote: {
                                        url: '<?php echo base_url();?>Welcome/nameCheck',
                                        type: 'POST',
                                        data: {
                                            name: function() {
                                                return $("#name").val();
                                            }
                                        }
                                	} 
                            },
					file_name: { required: true }
    		    },
    		    messages: {
                    name: { required: "Please enter a name",
							remote: "Name already exist" },
                    file_name: { required: "Please select a file" }
    		    },
    		    submitHandler: function(form) {
                    if($("#file_name").val() != ''){
                        var file=$("#file_name").get(0).files[0].name;
                        if(file){
                            var file_size = $("#file_name").get(0).files[0].size/1024;
                            if(file_size < 2048){
                                var ext = file.split('.').pop().toLowerCase();
                                if(ext!='txt' && ext!='doc' && ext!='docx' && ext!='pdf' && ext!='png' && ext!='jpeg' && ext!='jpg' && ext!='gif'){
                                    $("#add_file_error").html('<br><span>Invalid file format</span>');
                                    add_validation = 0;
                                    return;
                                }
                                add_validation = 1;
                            }else{
                                $("#add_file_error").html('<br><span>file size is too large. Maximum allotted size <?php echo 2048/(1024).' MB'; ?></span>');
                                add_validation = 0;
                                return;
                            }
                        }
                    }
    		    }
    		});

            $("form#add_dirctory_form").submit(function(e) {
            e.preventDefault();
            // alert(add_validation);
            if (add_validation == 1) {
                $.ajax({
                    url: '<?php echo base_url();?>Welcome/add_directory',
                    type: 'POST',
                    data: new FormData(this),
                    success: function(response) {
                        add_validation = 0;
                        var obj = JSON.parse(response);
                            if(obj.st == 1) {   
                                $('#myModal').modal('toggle');
								alert(obj.msg);
                                $.ajax({
                                    url: '<?php echo base_url();?>Welcome/load_directory_ajax',
                                    type: 'POST',
                                    data: { },
                                        success: function(response) {  
                                        $('#myTable').DataTable().destroy();
                                        $("#myTable").html(response);
                                        $("#myTable").DataTable({
                                            "searching": true,
                                            "bPaginate": true,
                                            "bInfo": true,
                                            "pageLength": 10,
                                            "aoColumnDefs": [
                                                {
                                                    'bSortable': false,
                                                    'aTargets': [0]
                                                },
                                                {
                                                    'bSortable': false,
                                                    'aTargets': [1]
                                                },
                                                {
                                                    'bSortable': false,
                                                    'aTargets': [2]
                                                },
                                                {
                                                    'bSortable': false,
                                                    'aTargets': [3]
                                                },
                                                {
                                                    'bSortable': false,
                                                    'aTargets': [4]
                                                }
                                            ]
                                        });    
                                    } 
                                }); 
                            } else {
                                alert(obj.msg);
                            }
                        
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
	});

    function deleteData(id){
        $.ajax({
            url: '<?php echo base_url();?>Welcome/deleteData',
            type: 'POST',
            data:{id:id},
            success: function(response) {
                var obj = JSON.parse(response);
                alert(obj.msg);
                $.ajax({
                    url: '<?php echo base_url();?>Welcome/load_directory_ajax',
                    type: 'POST',
                    data: { },
                        success: function(response) {  
                        $('#myTable').DataTable().destroy();
                        $("#myTable").html(response);
                        $("#myTable").DataTable({
                            "searching": true,
                            "bPaginate": true,
                            "bInfo": true,
                            "pageLength": 10,
                            "aoColumnDefs": [
                                {
                                    'bSortable': false,
                                    'aTargets': [0]
                                },
                                {
                                    'bSortable': false,
                                    'aTargets': [1]
                                },
                                {
                                    'bSortable': false,
                                    'aTargets': [2]
                                },
                                {
                                    'bSortable': false,
                                    'aTargets': [3]
                                },
                                {
                                    'bSortable': false,
                                    'aTargets': [4]
                                }
                            ]
                        });    
                    } 
                }); 
            }
        });
    }

	</script>