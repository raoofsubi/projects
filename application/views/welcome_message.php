<?php $this->load->view("includs/header");?>
<?php $this->load->view("js/welcome_script");?>
<body>
<div id="container">
	<h4>Directory List</h4>
	<hr>
	<button class="btn btn-success" data-toggle="modal" data-target="#myModal" style="float:right;">
        Add Directory
	</button>
	<a class="btn btn-info" href="<?php echo base_url();?>welcome/deleted_files">
		View deleted files
	</a>
	<div id="body">
		<div class="white_card">
			<table id="myTable" class="display">
    			<thead>
    			    <tr>
    			        <th>Sl.No</th>
    			        <th>Name</th>
    			        <th>File</th>
    			        <th>Action</th>
    			    </tr>
    			</thead>
    			<tbody>
				<?php $i=1; 
                foreach($directoryList as $list){ ?>
    				<tr>
    			        <td><?php echo $i;?></td>
    			        <td><?php echo $list['name'];?></td>
    			        <td><a class="btn mybutton mybuttoninfo" href="<?php echo base_url(); ?>uploads/files/<?php echo $list['file_name']?>" target="_blank">View file</a></td>
						<td>
                            <button class="btn btn-default option_btn btn-sm" title="Delete" onclick="deleteData(<?php echo $list['id'];?>)">
								Delete
							</button>
						</td>
    			    </tr>
				<?php $i++; } ?>
    			</tbody> 
			</table>
		</div>
	</div>
</div>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      	<div class="modal-content">
      	  	<div class="modal-header">
      	  	  <h4 class="modal-title">Add Directory</h4>
      	  	</div>
			<form id="add_dirctory_form" method="post" enctype="multipart/form-data">
      	  		<div class="modal-body">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            	        <div class="form-group">
            	            <label>Name<span style="color:red;">*</span></label>
            	            <input type="text" name="name" id="name" class="form-control"/>
            	        </div>
						<div class="form-group">
            	            <label>File<span style="color:red;">*</span></label>
            	            <input type="file" name="file_name" id="file_name" class="form-control"/>
            	        </div>
					</div>
					<div id="add_file_error" style="color:red;"> </div>
					<small>* Format: txt,doc,docx,pdf,png,jpeg,jpg,gif</small>
      	  		</div>
      	  		<div class="modal-footer">
					<button class="btn btn-success" type="submit">Save</button>
      	  		  	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	  		</div>
			</form>
      	</div>
    </div>
  </div>
</body>
</html>