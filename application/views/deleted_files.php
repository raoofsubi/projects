<?php $this->load->view("includs/header");?>
<?php $this->load->view("js/welcome_script");?>
<body>
<div id="container">
	<h4>Archive List</h4>
	<hr>
	<a class="btn btn-default" href="<?php echo base_url();?>">
		Back
	</a>
	<div id="body">
		<div class="white_card">
			<table id="myTable" class="display">
    			<thead>
    			    <tr>
    			        <th>Sl.No</th>
    			        <th>Name</th>
    			        <th>File</th>
    			    </tr>
    			</thead>
    			<tbody>
				<?php $i=1; 
                foreach($directoryList as $list){ ?>
    				<tr>
    			        <td><?php echo $i;?></td>
    			        <td><?php echo $list['name'];?></td>
    			        <td><a class="btn mybutton mybuttoninfo" href="<?php echo base_url(); ?>uploads/files/<?php echo $list['file_name']?>" target="_blank">View file</a></td>
    			    </tr>
				<?php $i++; } ?>
    			</tbody> 
			</table>
		</div>
	</div>
</div>
</body>
</html>