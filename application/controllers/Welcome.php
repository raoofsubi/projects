<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
        parent::__construct();
		$this->load->model("Welcome_model");
	}
	
	public function index() {
		$this->data['directoryList'] = $this->Welcome_model->getDirectory();
		$this->load->view('welcome_message', $this->data);
	}

	public function add_directory(){
		$name= $this->input->post('name');
		if($_FILES['file_name']['name'] != ''){
			$this->load->library('upload');
			$files = str_replace(' ', '_', $_FILES['file_name']);
			$_FILES['file_name']['name']     = $files['name'];
			$_FILES['file_name']['type']     = $files['type'];
			$_FILES['file_name']['tmp_name'] = $files['tmp_name'];
			$_FILES['file_name']['size']     = $files['size'];
			$ext = pathinfo($_FILES["file_name"]["name"])['extension'];
			$config['file_name'] = time();
			$config['upload_path']           = 'uploads/files/';
			$config['allowed_types']         = 'txt|doc|docx|pdf|png|jpeg|jpg|gif';
			$this->upload->initialize($config);
			$upload = $this->upload->do_upload('file_name');
			if($upload){
				$fileName = $config['file_name'].".".$ext; 
			}else{
				$data['st']= 2;
				$data['msg']= $this->upload->display_errors();
				print_r(json_encode($config));
				exit();
			}
			$dataArr = array('name'=>$name, 'file_name'=>$fileName);
			$res = $this->Welcome_model->saveDirectory($dataArr);
			if($res) {
				$data['st'] = 1;
				$data['msg'] = "Data added successfully";
			} else {
				$data['st'] = 0;
				$data['msg'] = 'Error while inserting data';
			}
		}
		print_r(json_encode($data));
	}

	public function load_directory_ajax() {
		$directoryList = $this->Welcome_model->getDirectory();
        $html = '<thead>
					<tr>
						<th>Sl.No</th>
						<th>Name</th>
						<th>File</th>
						<th>Action</th>
					</tr>
				</thead>';
        if(!empty($directoryList)) {
            $i=1; 
            foreach($directoryList as $list){
                $html.='<tr>
							<td>'.$i.'</td>
							<td>'.$list['name'].'</td>
							<td>
								<a class="btn mybutton mybuttoninfo" href="'.base_url().'uploads/files/'.$list['file_name'].'" target="_blank">View file</a>
							</td>
							<td>
								<button class="btn btn-default option_btn btn-sm" title="Delete" onclick="deleteData('.$list['id'].')">
									Delete
								</button>
							</td>
					</tr>';
					$i++;   
			}
        }
        
        echo $html;
	}
	
	public function nameCheck(){
		$name=$this->input->post('name');
		$query= $this->db->get_where('file_records',array('name'=>$name,'status'=> 1));
		if($query->num_rows()>0) {
		   echo 'false';
		} else {
			echo 'true';
		}
	}

	public function deleteData(){
		$id=$this->input->post('id');
		$res = $this->Welcome_model->deleteData($id);
		if($res) {
			$data['st'] = 1;
			$data['msg'] = "Data deleted successfully";
		} else {
			$data['st'] = 0;
			$data['msg'] = 'Error while deleteing data';
		}
		print_r(json_encode($data));
	}

	public function deleted_files() {
		$this->data['directoryList'] = $this->Welcome_model->getdeletedDirectory();
		$this->load->view('deleted_files', $this->data);
	}
}
